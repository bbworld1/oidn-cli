#include <OpenImageDenoise/oidn.hpp>
#include <iostream>

// Create denoise device
oidn::DeviceRef device = oidn::newDevice();
device.commit();

// Create filter
oidn::FilterRef filter = device.newFilter("RT");
filter.setImage("color", colorPtr, oidn::Format::Float3, width, height);
filter.setImage("normal", normalPtr, oidn::Format::Float3, width, height);
filter.setImage("output", outputPtr, oidn::Format::Float3, width, height);
filter.set("hdr", true);
filter.commit();

// Apply filter to image
filter.execute();

// Catch error
const char* errorMsg;
if (device.getError(errorMsg) != oidn::Error::None) {
	std::cout << "Error: " << errorMsg << std::endl;
}
